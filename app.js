const jwt = require('jsonwebtoken');
const server = require('http').createServer();
const io = require('socket.io')(server);
const database = require('./database');
const {
  SEND_DESTINATION_TO_SERVER_REQUEST,
  SEND_DESTINATION_TO_SERVER_SUCCESS,
  SUBSCRIBE_TO_PARKING_LOT_REQUEST,
  UNSUBSCRIBE_TO_PARKING_LOT_REQUEST,
  SEND_PARKING_LOT_CROWDEDNESS,
  UPDATED_PARKING_LOT_CROWDEDNESS,
  NEW_PARKING_LOT_REQUEST,
  NEW_PARKING_LOT_ERROR,
  NEW_PARKING_LOT_SUCCESS,
  DELETE_PARKING_LOT_REQUEST,
  DELETE_PARKING_LOT_ERROR,
  DELETE_PARKING_LOT_SUCCESS,
  LOGIN_SOCKET_IO_REQUEST,
  LOGIN_ERROR,
  LOGIN_SUCCESS,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
} = require('./actions/types');

const ParkingLotsManager = require('./models/parkingLotsManager');

io.origins('*:*');

io.sockets.on('connection', socket => {
  // eslint-disable-next-line no-param-reassign
  socket.auth = false;
  socket.on('action', action => {
    switch (action.type) {
      case LOGIN_SOCKET_IO_REQUEST:
        // eslint-disable-next-line no-unused-vars
        jwt.verify(action.payload, process.env.SECRET_KEY, (error, decoded) => {
          if (error) {
            socket.emit('action', {
              type: LOGIN_ERROR,
              payload: error,
            });
          } else {
            // eslint-disable-next-line no-param-reassign
            socket.auth = true;
            socket.emit('action', {
              type: LOGIN_SUCCESS,
              payload: action.payload,
            });
          }
        });
        break;
      case LOGOUT_REQUEST:
        // eslint-disable-next-line no-param-reassign
        socket.auth = false;
        socket.emit('action', {
          type: LOGOUT_SUCCESS,
          payload: {},
        });
        socket.disconnect(true);
        break;
      case NEW_PARKING_LOT_REQUEST:
        ParkingLotsManager.add(action.payload).then(result => {
          if (!result) {
            socket.emit('action', {
              type: NEW_PARKING_LOT_ERROR,
              payload: {},
            });
          } else {
            socket.emit('action', {
              type: NEW_PARKING_LOT_SUCCESS,
              payload: result,
            });
          }
        });
        break;
      case DELETE_PARKING_LOT_REQUEST:
        if (!ParkingLotsManager.delete(action.payload)) {
          socket.emit('action', {
            type: DELETE_PARKING_LOT_ERROR,
            payload: {},
          });
        } else {
          socket.emit('action', {
            type: DELETE_PARKING_LOT_SUCCESS,
            payload: {},
          });
        }

        break;
      case SEND_DESTINATION_TO_SERVER_REQUEST:
        socket.emit('action', {
          type: SEND_DESTINATION_TO_SERVER_SUCCESS,
          payload: ParkingLotsManager.getParkingLotsBasedOnRequest(action.payload),
        });
        break;
      case SUBSCRIBE_TO_PARKING_LOT_REQUEST:
        socket.join(action.payload);
        break;
      case UNSUBSCRIBE_TO_PARKING_LOT_REQUEST:
        socket.leave(action.payload);
        break;
      case SEND_PARKING_LOT_CROWDEDNESS:
        io.in(action.payload.room).emit('action', {
          type: UPDATED_PARKING_LOT_CROWDEDNESS,
          payload: ParkingLotsManager.changeCrowdednessOfParkingLotWithRoom(
            action.payload.room,
            action.payload.crowdedness
          ),
        });
        break;
      default:
        console.log(`Action type not recognized: '${action.type}'`);
        break;
    }
  });
});

database.sequelize.sync().then(() => {
  io.listen(process.env.PORT || 3000);
});
