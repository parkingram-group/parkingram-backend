# parkingram-backend

# Installation

- `npm install`
- The following environment variables must be provided:
  - `DATABASE_URL` (the URL of the PostgreSQL database)
  - `SECRET_KEY` (for the JWT)
- `npm start`
