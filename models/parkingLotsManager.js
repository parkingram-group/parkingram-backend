const {
  findAllParkingLotsPromise,
  findOrCreateParkingLotPromise,
  deleteParkingLotPromise,
} = require('../controllers/parkingLotController');
const ParkingLot = require('./parkingLotModel');

class ParkingLotsManager {
  constructor() {
    if (!ParkingLotsManager.instance) {
      this._data = [];
      findAllParkingLotsPromise().then(
        parkingLots => {
          parkingLots.forEach(element => {
            this._data.push(new ParkingLot(element));
          });
        },
        err => {
          console.log(err);
        }
      );
      ParkingLotsManager.instance = this;
    }

    return ParkingLotsManager.instance;
  }

  get parkingLots() {
    return this._data;
  }

  parkingLotWithLatitudeAndLongitude(latitude, longitude) {
    const { length } = this._data;
    for (let i = 0; i < length; i += 1) {
      if (this._data[i].latitude === latitude && this._data[i].longitude === longitude) {
        return this._data[i];
      }
    }
    return undefined;
  }

  changeCrowdednessOfParkingLotWithLatitudeAndLongitude(latitude, longitude, crowdedness) {
    const { length } = this._data;
    for (let i = 0; i < length; i += 1) {
      if (this._data[i].latitude === latitude && this._data[i].longitude === longitude) {
        this._data[i].crowdedness = crowdedness;
        return this._data[i].crowdedness;
      }
    }
    return 'unknown';
  }

  changeCrowdednessOfParkingLotWithRoom(room, crowdedness) {
    const { length } = this._data;
    for (let i = 0; i < length; i += 1) {
      if (this._data[i].room === room) {
        this._data[i].crowdedness = crowdedness;
        return this._data[i].crowdedness;
      }
    }
    return 'unknown';
  }

  async add(parkingLot) {
    const [parkingLotInDb, created] = await findOrCreateParkingLotPromise(parkingLot);
    if (created) {
      const newParkingLot = new ParkingLot(parkingLotInDb);
      this._data.push(newParkingLot);
      return newParkingLot.export();
    }
    return undefined;
  }

  delete(parkingLot) {
    let index = -1;
    for (let i = 0; i < this._data.length; i += 1) {
      if (parkingLot.id === this._data[i].id) {
        index = i;
        break;
      }
    }
    if (index > -1) {
      deleteParkingLotPromise(parkingLot)
        .then(() => {
          this._data.splice(index, 1);
          return true;
        })
        .catch(() => {
          return undefined;
        });
    }
    return undefined;
  }

  getAllParkingLotsForEmitter() {
    const result = [];
    this._data.forEach(element => {
      const key = element.room;
      const message = `location='${element.location}'&allSlots='${element.slots}'&crowdedness='${element.crowdedness}'`;
      result.push({ key, msg: message });
    });
    return result;
  }

  getParkingLotsBasedOnRequest(request) {
    const {
      destination,
      currentLocation,
      maxDistanceFromDestination,
      maxNumberOfParkingLots,
    } = request;
    const result = [];
    for (let i = 0; result.length <= maxNumberOfParkingLots && i < this._data.length; i += 1) {
      const distanceFromDestination = this._data[i].distance(
        destination.latitude,
        destination.longitude
      );
      if (distanceFromDestination <= maxDistanceFromDestination) {
        const distanceFromCurrentLocation = this._data[i].distance(
          currentLocation.latitude,
          currentLocation.longitude
        );
        result.push({ ...this._data[i].export(), distance: distanceFromCurrentLocation });
      }
    }
    return result;
  }
}

const instance = new ParkingLotsManager();
Object.freeze(instance);

module.exports = instance;
