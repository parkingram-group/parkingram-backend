const shortid = require('shortid');
const geolib = require('geolib');

const crowdednessOptions = {
  UNKNOWN: 'unknown',
  EMPTY: 'empty',
  FEW_OCCUPIED_PLACES: 'few occupied places',
  HALF_FULL: 'half full',
  CROWDED: 'crowded',
  FULL: 'full',
};
module.exports = class ParkingLot {
  constructor(informationFromDb) {
    this._id = informationFromDb.id;
    this._latitude = informationFromDb.latitude;
    this._longitude = informationFromDb.longitude;
    this._address = informationFromDb.address;
    this._name = informationFromDb.name;
    this._slots = informationFromDb.slots;
    this._room = shortid.generate();
    this._crowdedness = crowdednessOptions.UNKNOWN;
    this._certainty = 1;
  }

  get id() {
    return this._id;
  }

  get latitude() {
    return this._latitude;
  }

  get longitude() {
    return this._longitude;
  }

  get location() {
    return `${this._latitude}&${this._longitude}`;
  }

  get address() {
    return this._address;
  }

  get name() {
    return this._name;
  }

  get slots() {
    return this._slots;
  }

  get room() {
    return this._room;
  }

  get crowdedness() {
    return this._crowdedness;
  }

  set crowdedness(value) {
    const ok = Object.keys(crowdednessOptions).filter(key => {
      return Object.is(crowdednessOptions[key], value);
    });
    if (ok) this._crowdedness = value;
  }

  get certainty() {
    return this._certainty;
  }

  export() {
    return {
      id: this._id,
      latitude: this._latitude,
      longitude: this._longitude,
      address: this._address,
      name: this._name,
      slots: this._slots,
      room: this._room,
      crowdedness: this._crowdedness,
      certainty: this._certainty,
    };
  }

  distance(latitude, longitude) {
    if (this._latitude === latitude && this._longitude === longitude) {
      return 0;
    }
    return (
      geolib.getDistance(
        { latitude: this._latitude, longitude: this._longitude },
        {
          latitude,
          longitude,
        }
      ) / 1000
    );
  }
};
