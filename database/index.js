const Sequelize = require('sequelize');

const sequelize = new Sequelize(process.env.DATABASE_URL, {
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  define: {
    timestamps: false,
    underscored: true,
  },
});

const models = {
  ParkingLotDb: sequelize.import('./parkingLotDb'),
};

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = models;
