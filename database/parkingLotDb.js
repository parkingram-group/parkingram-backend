module.exports = (sequelize, DataTypes) => {
  const ParkingLotDb = sequelize.define('parking_lots', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true,
    },
    latitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'latitude',
    },
    longitude: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      field: 'longitude',
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'address',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name',
    },
    // -1, if the value is unknown
    slots: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'slots',
    },
  });

  return ParkingLotDb;
};
