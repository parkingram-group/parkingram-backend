exports.modelToDto = parkingLot => {
  return {
    id: parkingLot.dataValues.id,
    latitude: parkingLot.dataValues.latitude,
    longitude: parkingLot.dataValues.longitude,
    address: parkingLot.dataValues.address,
    name: parkingLot.dataValues.name,
    slots: parkingLot.dataValues.slots,
  };
};

exports.modelsToDtos = parkingLots => {
  const result = [];
  for (let i = 0; i < parkingLots.length; i += 1) {
    result.push({
      id: parkingLots[i].dataValues.id,
      latitude: parkingLots[i].dataValues.latitude,
      longitude: parkingLots[i].dataValues.longitude,
      address: parkingLots[i].dataValues.address,
      name: parkingLots[i].dataValues.name,
      slots: parkingLots[i].dataValues.slots,
    });
  }
  return result;
};

exports.dtoToModel = parkingLot => {
  return {
    latitude: parkingLot.latitude,
    longitude: parkingLot.longitude,
    address: parkingLot.address,
    name: parkingLot.name,
    slots: parkingLot.slots,
  };
};
