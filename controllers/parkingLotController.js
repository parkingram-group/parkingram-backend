const { ParkingLotDb } = require('../database');
const parkingLotAssembler = require('../assemblers/parkingLotAssembler');

exports.findAllParkingLotsPromise = () => {
  return new Promise((resolve, reject) => {
    ParkingLotDb.findAll().then((parkingLots, err) => {
      if (err) {
        reject(new Error('Internal error'));
      } else {
        resolve(parkingLotAssembler.modelsToDtos(parkingLots));
      }
    });
  });
};

exports.findOrCreateParkingLotPromise = newParkingLot => {
  return new Promise((resolve, reject) => {
    ParkingLotDb.findOrCreate({ where: parkingLotAssembler.dtoToModel(newParkingLot) })
      .then(([parkingLot, created]) => {
        resolve([parkingLotAssembler.modelToDto(parkingLot), created]);
      })
      .catch(error => {
        reject(error);
      });
  });
};

exports.deleteParkingLotPromise = parkingLotToDelete => {
  return new Promise((resolve, reject) => {
    const { id } = parkingLotToDelete;
    ParkingLotDb.findByPk(id).then(parkingLot => {
      if (!parkingLot) {
        reject(Error('No parking lot with the given id'));
      } else {
        parkingLot.destroy({ force: true });
        resolve({});
      }
    });
  });
};
